const board = document.querySelector('.game');
const BOARD_SIZE = 700
const DEFAULT_SIZE = 50;
let currentSize = DEFAULT_SIZE

function createBoard(squares) {
    for (let i = 0; i < squares; i++) {
        let row = document.createElement('div');
        row.classList.add('row');
        for (let j = 0; j < squares; j++) {
            let square = document.createElement('div')
            square.classList.add('square');
            square.addEventListener('mouseover', () => {
                square.style.backgroundColor= "black";
            })
            row.appendChild(square)
        }
        board.appendChild(row)
    }
}

function clearBoard() {
    board.innerHTML = ""
}

function reloadBoard() {
    clearBoard()
    createBoard(currentSize)
}

function changeSize() {
    var input = parseInt(prompt("Enter a number between 1 and 100"));

    if (input < 1 || input > 100) {
        alert("Invalid number: " + input)
    } else {
        currentSize = input
        reloadBoard()
    }
}

window.onload = () => {
    createBoard(DEFAULT_SIZE);

    const clearButton = document.getElementById('clear');
    const sizeButton = document.getElementById('size');

    clearButton.addEventListener('click', reloadBoard)
    sizeButton.addEventListener('click', changeSize)
}